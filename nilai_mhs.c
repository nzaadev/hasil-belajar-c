#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(){
	
	int pilihan;


	float n_h, n_t, n_q, n_a, n_u;
	
	char nim[5][10],nama[5][12],matkul[5][5] ;
	
	int absen = 0;
	int step;

	float n_hadir[5],n_tugas[5],n_quiz[5],n_aktif[5],n_uas[5],n_total[5] ;
	
	while(1){
		puts("\n--------------- INPUT DATA MULTI by Imanz ---------------------\n");
		puts("[1] INPUT DATA MAHASISWA");
		puts("[2] INPUT NILAI MAHASISWA");
		puts("[3] TAMPIL SEMUA DATA MAHASISWA");
		puts("[4] EXIT PROGRAM");
		puts("------------------------------------------------------");
		printf("\n [?] Masukkan pilihan: " );
		scanf(" %d", &pilihan);
	
		switch(pilihan){
				case 1:
				if(step == 2)
				absen++;
			
				puts("\n------- INPUT DATA BARU MAHASISWA ----------\n");
				ulang:
				printf("\n [+] Masukkan Nama: ");
				scanf(" %s", nama[absen]);
				
				for(int a=0; a<absen; a++){
				
						if(strcasecmp(nama[a],nama[absen]) == 0){
						 puts("\n[!] Nama ini sudah ada");
						 goto ulang;
						}
				}
				
				printf("\n [+] Masukkan NIM: ");
				scanf(" %s", nim[absen]);
				printf("\n [+] Masukkan Matkul: ");
				scanf(" %s", matkul[absen]);
				step = 1;
				break;
				case 2:
				if (step != 1 )
					puts("\n [!] Silahkan input data mahasiswa dulu\n");
				else {
				printf("\n-------- INPUT NILAI UNTUK %s -----------\n", nama[absen]);
				printf(" [+] Nilai Hadir: ");
				scanf(" %f", &n_h);
				
				printf("\n [+] Nilai Tugas: ");
				scanf(" %f", &n_t);
				
				printf("\n [+] Nilai Quiz: ");
				scanf(" %f", &n_q);
				
				printf("\n [+] Nilai Forum: ");
				scanf(" %f", &n_a);
				
				printf("\n [+] Nilai UAS: ");
				scanf(" %f", &n_u);
				
				n_hadir[absen] = n_h*((float)10/(float)100);
				n_tugas[absen] = n_t*((float)20/(float)100);
				n_quiz[absen] = n_q*((float)10/(float)100);
				n_aktif[absen] = n_a*((float)10/(float)100);
				n_uas[absen] = n_u*((float)50/(float)100);
				
				n_total[absen] = n_hadir[absen] + n_tugas[absen] + n_quiz[absen] + n_aktif[absen] + n_uas[absen];
				
				
				step = 2;
				}
				
				break;
				
				case 3:
				
				if(step == 2)
				absen++;
				else
				puts("\n[!] Anda belum memasukkan data nilai\n");
				
				puts("\n ----------- DATA SEMUA MAHASISWA -----------------\n");
				
				
				for(int i=0; i<absen; i++){
					
					printf(" = Nama: %s\n", nama[i]);
					printf(" = NIM: %s\n", nim[i]);
					printf(" = Nilai Total: %1.f\n", n_total[i]);
					printf(" = Kelulusan: ");
					
					if(n_total[i] >= 90)
					puts("Grade A (Lulus)");
					else if(n_total[i] >= 80 && n_total[i] < 90)
					puts("Grade B (lulus)");
					else if(n_total[i] >= 70 && n_total[i] < 80)
					puts("Grade C (Lulus)");
					else if(n_total[i] >= 60 && n_total[i] < 70)
					puts("Grade D (Lulus)");
					else
					puts("Tidak lulus");
				puts("--------------------------------");
				}
				
				step = 3;
				
				break;
				
				case 4:
				default:
				return 0;
				break;
				
				
		}
	
	// end while
		
	
	}
	
	return 0;
}

