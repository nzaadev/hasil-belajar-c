#include <stdio.h>
#include <stdbool.h>
#include <string.h>

struct penilaian{
    float hadir;
    float tugas;
    float quiz;
    float keaktifan;
    float uas;
    };

int main(void)
{
    struct penilaian nilai;
  //  float tugas,quiz,keaktifan,uas;
    char ceknim [20];
    char ceknama [40];
    char cekmatkul [40];
    char pil[5];
    bool exit = false;
    bool cek1 = false;
    bool cek2 = false;
    int pilihan;
    do{

        printf( "============================\n");
        printf( "SISTEM PENILAIAN MATA KULIAH\n");
        printf( "\n");
        printf( "Pilihan Menu: \n");
        printf( "1. Input Data Mahasiswa \n");
        printf( "2. Input Nilai \n");
        printf( "3. Lihat Nilai dan Status \n");
        printf( "4. Exit \n");
        printf( "Pilihan Anda: ");
        scanf("%d", &pilihan);
        if(pilihan==1){
            char nim[20];
            char nama[40];
            char matkul[40];
            char pill[5];
            int cek_nama,cek_nim,cek_pil;
            printf( "== Menu Input Data==\n");
            printf( "Masukan Nim Anda: ");
            scanf("%s",nim);
            printf( "Masukan Nama Anda: ");
            gets(nama);
            gets(nama);
            printf( "Masukan Mata Kuliah: ");
            gets(matkul);
            cek_nama= strcmp(ceknama,nama);
            cek_nim = strcmp(ceknim,nim);
            if(cek_nama == 0 || cek_nim == 0){
                strcpy(pil,"Y");
                printf( "Nim atau Nama yang Anda masukan Sudah Dipakai\n");
                printf( "Apakah Anda ingin menggantinya? (Y/T) ") ;
                gets(pill);
                cek_pil = strcmp(pil,pill);
                if(cek_pil == 0){
                    strcpy(ceknim, nim);
                    strcpy(ceknama, nama);
                    strcpy(cekmatkul, matkul);
                    printf( "Data Sudah Terupdate\n");
                }else{
                    printf( "Data Tetap Pada Identitas Awal\n");
                }
            }else{
                strcpy(ceknim, nim);
                strcpy(ceknama, nama);
                strcpy(cekmatkul, matkul);
            }
            cek2 = true;
            printf( " \n");
            printf( " \n");
        }else if(pilihan==2){
            if(cek2!=true){
                printf( "== Menu Input Nilai==\n");
                printf( "Belum Ada Identitas Yang Dimasukan. Silahkan Inputkan data terlebih dahulu pada menu 1\n");
            }else{
                printf( "== Menu Input Nilai==\n");
                printf("Masukkan Nilai Hadir           : ");
                scanf("%f",&(nilai.hadir));
                printf("Masukkan Nilai Tugas           : ");
                scanf("%f",&(nilai.tugas));
                printf("Masukkan Nilai Quiz            : ");
                scanf("%f",&(nilai.quiz));
                printf("Masukkan Nilai Keaktifan Forum : ");
                scanf("%f",&(nilai.keaktifan));
                printf("Masukkan Nilai UAS             : ");
                scanf("%f",&(nilai.uas));
                cek1 = true;
                printf( "Nilai Sudah Diinputkan\n");
            };

            printf(" \n");
            printf(" \n");
        }else if(pilihan==3){
            float nilaiakhir;
            char status1[5]= "Lulus";
            char status2[12]= "Tidak Lulus";
            if(cek1!=true){
                printf("== Menu Info Nilai==\n");
                printf("Belum Ada Nilai Yang Dimasukan. Silahkan Input nilai terlebih dahulu pada menu 2 \n");
            }else{
                printf("== Menu Info Nilai==\n");
                printf("Nim         : %s",ceknim);
                printf("\nNama        : %s",ceknama);
                printf("\nMata Kuliah : %s",cekmatkul);
                nilaiakhir = nilai.hadir * 0.10 + nilai.tugas * 0.20 + nilai.quiz * 0.10 + nilai.keaktifan * 0.10 + nilai.uas * 0.50;
                if(nilaiakhir>=90 && nilaiakhir<=100){
                    char grade = 'A';
                    printf("\nNilai Akhir : %.2f",nilaiakhir);
                    printf("\nGrade       : %c",grade);
                    printf("\nStatus      : %s",status1);
                }else if(nilaiakhir>=80 && nilaiakhir<=89){
                    char grade = 'B';
                    printf("\nNilai Akhir : %.2f",nilaiakhir);
                    printf("\nGrade       : %c",grade);
                    printf("\nStatus      : %s",status1);
                }else if(nilaiakhir>=70 && nilaiakhir<=79){
                    char grade = 'C';
                    printf("\nNilai Akhir : %.2f",nilaiakhir);
                    printf("\nGrade       : %c",grade);
                    printf("\nStatus      : %s",status1);
                }else if(nilaiakhir>=60 && nilaiakhir<=69){
                    char grade = 'D';
                    printf("\nNilai Akhir : %.2f",nilaiakhir);
                    printf("\nGrade       : %c",grade);
                    printf("\nStatus      : %s",status1);
                }else if(nilaiakhir>=50 && nilaiakhir<=59){
                    char grade = 'E';
                    printf("\nNilai Akhir : %.2f",nilaiakhir);
                    printf("\nGrade       : %c",grade);
                    printf("\nStatus      : %s",status2);
                }else{
                    printf("\nTidak Ada Grade (Nilai Terlalu Rendah)");
                }
            }
            printf( " \n");
            printf( " \n");
        }else if(pilihan==4){
            exit = true;
        }else{
            printf( "Key Yang Anda Masukan Salah, Silahkan Input Kembali\n");
        }
    }while (exit != true);
    return 0;
}


